## This repo is for making ready to use Jenkins inside container
Just build image and run it as a container.

The next docker installer code has got from the Rancher site (https://rancher.com/docs/rancher/v1.6/en/hosts/#supported-docker-versions)

`curl https://releases.rancher.com/install-docker/18.06.sh > v`

### Image create example
It strictly recommended to use the same jenkins **version** as in the FROM Dockerfile section.

    docker build --tag=show4me/jenkins:2.150.3 .

### Docker run example
Replace ${VARIABLES} by correct names.

    docker run  \
    --privileged \
    -p 0.0.0.0:${PUBLISHED_PORT}:8080 \
    --detach \
    --cpus=3 \
    --memory=3500m \
    --restart=unless-stopped \
    --user=root \
    --network=${CONTAINERS_DOCKER_NET_NAME} \
    \
    --name=${CONTAINER_NAME} \
    --mount type=bind,source=/var/run/docker.sock,target=/var/run/docker.sock \
    --mount type=bind,source=/etc/localtime,target=/etc/localtime,readonly \
    --mount type=volume,source=${MAIN_VOLUME},target=/var/jenkins_home \
    ${JENKINS_IMAGE}
    
### Push image to a certain DockerHub repo

    docker push show4me/jenkins:2.150.3
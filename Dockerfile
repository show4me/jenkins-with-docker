# Jenkins Dockerfile with a Docker installation
# ==================

FROM jenkins/jenkins:2.150.3
ARG DOCKERINSTALLER="18.06.sh"

COPY /${DOCKERINSTALLER} /bin/

USER root
RUN chmod a+x /bin/${DOCKERINSTALLER} && ./bin/${DOCKERINSTALLER}
USER jenkins